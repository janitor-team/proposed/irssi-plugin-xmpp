irssi-plugin-xmpp (0.54+git20191101+c13fa5-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * d/p/gtimeval-build-fix.patch: cherry-picked from a PR upstream to fix
    build against newer irssi (Closes: #1015031)

 -- Simon Chopin <schopin@ubuntu.com>  Tue, 02 Aug 2022 23:53:15 +0200

irssi-plugin-xmpp (0.54+git20191101+c13fa5-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Repository, Repository-Browse.
  * Rely on pre-initialized dpkg-architecture variables.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository.

  [ Florian Schlichting ]
  * Import new upstream version 0.54+git20191101+c13fa5 (closes: 931886)
  * Bump debhelper-compat to level 13
  * Refresh patches, drop those applied upstream and update
    Implement-XEP-0280-Message-Carbons.patch to upstream PR#25
  * Add Rules-Requires-Root: no
  * Declare compliance with Debian Policy 4.5.1

 -- Florian Schlichting <fsfs@debian.org>  Fri, 22 Jan 2021 21:28:54 +0800

irssi-plugin-xmpp (0.54-3) unstable; urgency=medium

  * drop -dbg→-dbgsym transition (no longer needed)
  * capture irssi ABI dependency (Closes: #925596)
  * add myself to uploaders, with Florian's permission

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 27 Mar 2019 12:57:14 +0100

irssi-plugin-xmpp (0.54-2.1) unstable; urgency=medium

  * Non-maintainer upload, rebuilding against newer irssi
    (Closes: #772479)

  [ Florian Schlichting ]
  * Move packaging to salsa.d.o

  [ Daniel Kahn Gillmor ]
  * d/copyright: use https and fix upstream git
  * d/watch: use version 4, look at github releases
  * d/clean: clean up generated object files
  * introduce brittle versioning to roughly match irssi ABI

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 19 Sep 2018 10:14:11 -0400

irssi-plugin-xmpp (0.54-2) unstable; urgency=medium

  * Rename ban and kick help pages to not conflict with irssi
    (closes: #877910)

 -- Florian Schlichting <fsfs@debian.org>  Sat, 07 Oct 2017 09:18:25 +0200

irssi-plugin-xmpp (0.54-1) unstable; urgency=medium

  * Update VCS and Homepage URIs
  * Mark Multi-Arch: same, as suggested by the Multiarch hinter
  * Import upstream version 0.54
  * refresh patches
  * Add a patch series from github/ailin-nemui for muc-admin support
  * Add singpolyma-null-resource-was-causing-crash-here.patch
  * Update copyright years
  * Bump dh compat to level 10
  * Declare compliance with Debian Policy 4.1.1
  * Fix a "comparison between pointer and zero character constant" warning
  * Install converted README.md

 -- Florian Schlichting <fsfs@debian.org>  Sat, 07 Oct 2017 00:59:07 +0200

irssi-plugin-xmpp (0.53-1) unstable; urgency=medium

  * Import Upstream version 0.53
  * Refresh patches
  * Transition to automatic dbgsym package

 -- Florian Schlichting <fsfs@debian.org>  Sat, 19 Mar 2016 00:53:02 +0100

irssi-plugin-xmpp (0.52+git20160118-2) unstable; urgency=medium

  * Rebuild for new irssi version. This is #772479 again...
  * Use all hardening buildflags
  * Install modules in multiarch directory (closes: #817171)
  * Use secure URIs for Vcs-* fields
  * Declare compliance with Debian Policy 3.9.7

 -- Florian Schlichting <fsfs@debian.org>  Mon, 14 Mar 2016 23:20:10 +0100

irssi-plugin-xmpp (0.52+git20160118-1) unstable; urgency=medium

  * Import Upstream version 0.52+git20160118
  * Add XEP-0280 Message Carbons patch from singpolyma
  * Drop patches applied upstream:
    + useless-dependency-on-libidn.patch
    + hardening.patch
    + singpolyma-0201-If-the-resource-is-blank-do-not-include-it.patch
    + require-starttls.patch
    + set_ssl-conflicting-declarations.patch
  * Add singpolyma-fix-warnings.patch to address four compiler warnings
  * Update debian/copyright

 -- Florian Schlichting <fsfs@debian.org>  Mon, 18 Jan 2016 22:39:00 +0100

irssi-plugin-xmpp (0.52+git20140102-3) unstable; urgency=medium

  * Update XMPP-PGP support (closes: #779156)
  * Add strip_resource_08082009.patch to optionally ignore random resource
    strings
  * Bump Standards-Version to 3.9.6 (no changes necessary)

 -- Florian Schlichting <fsfs@debian.org>  Fri, 27 Mar 2015 21:44:52 +0100

irssi-plugin-xmpp (0.52+git20140102-2) unstable; urgency=medium

  * Add require-starttls.patch to ensure encrypted connections and prevent
    ssl-stripping attacks (closes: #754839). Thanks dkg for the patch!
  * Fix conflicting declarations of set_ssl and ensure encryption when
    registering (closes: #749411)

 -- Florian Schlichting <fsfs@debian.org>  Tue, 15 Jul 2014 15:01:05 +0200

irssi-plugin-xmpp (0.52+git20140102-1) unstable; urgency=medium

  [ Florian Schlichting ]
  * Import Upstream version 0.52+git20140102
  * Add VCS-* fields for collab-maint on alioth
  * Add upstream git URL to Source field in debian/copyright
  * Drop patches plucked from upstream CVS
  * Refresh hardening.patch (offset, drop hunk fixed upstream)
  * Provide xmpp-admin.pl script by Seth Difley
  * Add GTalk-MUC-support.patch, plucked from github/freemandrew
  * Add support for XMPP-PGP, plucked from github/singpolyma
  * New useless-dependency-on-libidn.patch, to fix a lintian warning
  * Declare compliance with Debian Policy 3.9.5

 -- Florian Schlichting <fsfs@debian.org>  Fri, 03 Jan 2014 00:25:20 +0100

irssi-plugin-xmpp (0.52-2) unstable; urgency=low

  * Added hardening.patch to actually use the dpkg-buildflags.
  * Dropped version on irssi(-dev) dependency
  * Email change: Florian Schlichting -> fsfs@debian.org
  * Patches plucked from upstream CVS:
    + kill-the-stroneline-function (closes: #693079)
    + fix-crash-on-empty-resource
    + replace-deprecated-g_strncasecmp

 -- Florian Schlichting <fsfs@debian.org>  Mon, 06 May 2013 00:03:37 +0200

irssi-plugin-xmpp (0.52-1) unstable; urgency=low

  * New Maintainer (closes: #667987).
  * Imported Upstream version 0.52 (closes: #661302).
    + fixing MUC invites to be compliant (closes: #609530).
  * Bumped debhelper compatibility to level 9.
  * Refreshed debian/copyright, switching to copyright-format 1.0.
  * Bumped Standards-Version to 3.9.3 (no further changes).
  * Renamed docs to irssi-plugin-xmpp.docs, include all of docs/, TODO.
  * Switched to short debian/rules.
  * Added a watch file.
  * Added pkg-config to build-dependencies.
  * Added Homepage field to source package control paragraph.

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Sun, 08 Apr 2012 15:34:43 +0200

irssi-plugin-xmpp (0.51+cvs20100627-1) unstable; urgency=low

  * New upstream release + CVS snapshot (Closes: #588163).
    - Prompt for a password if none was provided in the configuration
      (Closes: #580659).
    - Don't segfault upon reception of a ping without "from" field.
      (Closes: #521227).
    - Various bugfixes and improvements.
  * Provide a package with debugging symbols (Closes: #580658).
  * Bump Standards-Version to 3.8.4 (no changes needed).

 -- David Ammouial <da-deb@weeno.net>  Tue, 06 Jul 2010 15:14:51 +0200

irssi-plugin-xmpp (0.50+cvs20100122-1) unstable; urgency=low

  * New CVS snapshot that allows connections to talk.google.com
    (Closes: #565581).

 -- David Ammouial <da-deb@weeno.net>  Fri, 22 Jan 2010 16:09:55 -0600

irssi-plugin-xmpp (0.50+cvs20100105-1) unstable; urgency=low

  * New CVS snapshot.
    - Delayed Delivery (XEP-0203) support.
    - Account registration support.
    - Support for /CYCLE command, similar to IRC's.
    - Modification of "/roster add" behaviour.
    - TLS is used for connecting when available (Closes: #526356).
    - Use a timeout on connections to prevent frozen connections.
  * Bump Standards-Version to 3.8.3 (no changes).
  * Fix handling of CFLAGS environment in upstream Makefile (many thanks
    to Raphaël Hertzog).

 -- David Ammouial <da-deb@weeno.net>  Wed, 06 Jan 2010 05:11:18 -0600

irssi-plugin-xmpp (0.13+cvs20090617-1) unstable; urgency=low

  * New CVS snapshot:
    - Some bugfixes and code cleanup.
    - Allow to build on ia64 (Closes: #530304).
  * Bump Standards-Version to 3.8.2 (no changes).

 -- David Ammouial <da-deb@weeno.net>  Fri, 19 Jun 2009 23:14:39 -0500

irssi-plugin-xmpp (0.13+cvs20090406-1) unstable; urgency=low

  * New CVS snapshot, built against irssi-dev 0.8.13:
    - New features and bugfixes.
    - Fix segfault when successfully identified (Closes: #521227).
    - Fix build error (Closes: #527697)
  * Depend on irssi >=0.8.13, build-depend on irssi-dev >=0.8.13.
  * Bump Standards-Version to 3.8.1 (no changes needed).
  * Add INTERNAL to documentation files.

 -- David Ammouial <da-deb@weeno.net>  Tue, 12 May 2009 12:14:44 -0500

irssi-plugin-xmpp (0.13+cvs20080610-1) unstable; urgency=low

  * New CVS snapshot:
    - New features and bugfixes.
    - Fix ignoring of messages without a type attribute (Closes: #477989).
  * Depend on irssi >=0.8.12 (Closes: #469923).
  * Add FAQ and MUC to documentation files (Closes: #485595).
  * debian/rules: install help files in irssi help directory.
  * debian/rules: Remove useless "make -n" command in build-stamp rule.
  * Bump Standards-Version to 3.8.0 (no changes needed).
  * debian/control: Add "Enhances: irssi".

 -- David Ammouial <da-deb@weeno.net>  Mon, 23 Jun 2008 16:03:33 +0200

irssi-plugin-xmpp (0.13+cvs20080121-1) unstable; urgency=low

  * CVS snapshot.
  * Rephrased package description, thanks to Ted Percival (Closes: #444109).
  * debian/control: Promote "Homepage" to a real field.
  * Update homepage.
  * Adapt debian/rules to new upstream Makefile system.
  * Bump Standards-Version to 3.7.3 (no changes required).
  * Removed empty TODO from debian/docs, added docs/GENERAL, docs/STARTUP,
    docs/XEP.

 -- David Ammouial <da-deb@weeno.net>  Mon, 04 Feb 2008 21:01:27 +0100

irssi-plugin-xmpp (0.13-1) unstable; urgency=low

  * Initial release (Closes: #440017).

 -- David Ammouial <da-deb@weeno.net>  Wed, 29 Aug 2007 10:59:29 +0200
