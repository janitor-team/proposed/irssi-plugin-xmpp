Source: irssi-plugin-xmpp
Maintainer: Florian Schlichting <fsfs@debian.org>
Uploaders: Daniel Kahn Gillmor <dkg@fifthhorseman.net>
Section: net
Priority: optional
Build-Depends: debhelper-compat (= 13),
               irssi-dev (>= 1.1.0~),
               libloudmouth1-dev,
               pkg-config
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/debian/irssi-plugin-xmpp
Vcs-Git: https://salsa.debian.org/debian/irssi-plugin-xmpp.git
Homepage: https://cybione.org/~irssi-xmpp/
Rules-Requires-Root: no

Package: irssi-plugin-xmpp
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${irssi:ABI}
Enhances: irssi
Description: XMPP plugin for irssi
 An irssi plugin to connect to the Jabber network, using the XMPP protocol.
 .
 Its main features are:
  - Sending and receiving messages in irssi's query windows
  - A roster with contact & resource tracking (contact list)
  - Contact management (add, remove, manage subscriptions)
  - Tab completion of commands, JIDs and resources
  - Support for multiple accounts
  - Unicode support (UTF-8)
  - SSL support
